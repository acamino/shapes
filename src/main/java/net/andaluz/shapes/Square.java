package net.andaluz.shapes;

public class Square implements Shape {

    private int width;

    public Square(int width) throws InvalidShapeException {

        if (width < 0) {
            throw new InvalidShapeException("Invalid shape");
        }

        this.width = width;
    }

    public int perimenter() {
        return width * 4;
    }

    public double area() {
        return Math.pow(width, 2);
    }

    // Spike
    public void print() {
        for (int line = 0; line < width; line++) {
            if (line == 0 || line == width - 1) {
                System.out.println(buildTopOrBottom());
            } else {
                System.out.println(buildSegment());
            }
        }
    }

    private String buildTopOrBottom() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int index = 0; index < width; index++) {
            if (index == 0 || index == width - 1) {
                stringBuilder.append("+");
            } else {
                stringBuilder.append("--");
            }
        }

        return stringBuilder.toString();
    }

    private String buildSegment() {
        StringBuilder stringBuilder = new StringBuilder();

        for (int index = 0; index < width; index++) {
            if (index == 0 || index == width - 1) {
                stringBuilder.append("|");
            } else {
                stringBuilder.append("  ");
            }
        }

        return stringBuilder.toString();
    }
}
