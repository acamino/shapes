package net.andaluz.shapes;

public class App {

    public static void main(String[] args) throws InvalidShapeException {

        // Square
        Square square = new Square(10);

        System.out.println("Square");
        System.out.println(square.perimenter());
        System.out.println(square.area());

        square.print();

        // Rectangle
        Shape rectangle = new Rectangle(10, 5);

        System.out.println("Rectangle");
        System.out.println(rectangle.perimenter());
        System.out.println(rectangle.area());
    }
}
