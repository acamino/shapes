package net.andaluz.shapes;

public class Rectangle implements Shape {

    private int width;
    private int height;

    public Rectangle(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public int perimenter() {
        return 2 * width + 2 * height;
    }

    public double area() {
        return width * height;
    }
}
