package net.andaluz.shapes;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(Parameterized.class)
public class RectangleTest {

    private int width;
    private int height;
    private int expectedPerimeter;
    private double expectedArea;

    public RectangleTest(int width, int height, int expectedPerimeter, double expectedArea) {
        this.width = width;
        this.height = height;
        this.expectedPerimeter = expectedPerimeter;
        this.expectedArea = expectedArea;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { 0, 0, 0, 0 },
                { 2,  4, 12, 8 }
        });
    }

    @Test
    public void testComputationsOnASquare() throws Exception {
        // Arrange
        Rectangle rectangle = new Rectangle(width, height);

        // Act
        int perimeter =  rectangle.perimenter();
        double area = rectangle.area();

        // Assert
        assertThat(perimeter, is(expectedPerimeter));
        assertThat(area, is(expectedArea));
    }
}