package net.andaluz.shapes;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(Parameterized.class)
public class SquareTest {

    private int width;
    private int expectedPerimeter;
    private double expectedArea;

    public SquareTest(int width, int expectedPerimeter, double expectedArea) {
        this.width = width;
        this.expectedPerimeter = expectedPerimeter;
        this.expectedArea = expectedArea;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { 0, 0, 0 },
                { 2, 8, 4 },
                { 6, 24, 36 },
                { 10, 40, 100 }
        });
    }

    @Test
    public void testComputationsOnASquare() throws Exception {
        // Arrange
        Square square = new Square(width);

        // Act
        int perimeter =  square.perimenter();
        double area = square.area();

        // Assert
        assertThat(perimeter, is(expectedPerimeter));
        assertThat(area, is(expectedArea));
    }
}